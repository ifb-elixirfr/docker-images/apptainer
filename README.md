# Docker apptainer

This Docker image is mainly used to build Apptainer images with a CI.

## Build
To build a Docker image with the last version of Apptainer, just launch the CI pipeline from the branch `main` and 🤞

## Usage
```bash
docker run registry.gitlab.com/ifb-elixirfr/docker-images/apptainer:1.2.5 build --help
```